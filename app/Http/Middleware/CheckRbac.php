<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\Admin\Role;
use Route;
class CheckRbac
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // 先排除超级用户
        $role_id = Auth::guard('admin') -> user() -> role_id;
        if($role_id > '1'){
            // 需要进行权限认证
            $ac = Role::where('id',$role_id) -> value('auth_ac');
            // 拼接固定路由(固定的，谁都可以访问)
            $ac .= "IndexController@index,IndexController@welcome";
            // 获取当前用户的操作方法(获取url地址)
            $current = Route::currentRouteAction();
            // 将路由转换为数组
            $curr = explode('\\',$current);
            // 将2个自妇产进行比较，stripos不区分大小写
            if(stripos($ac,end($curr)) === false){
                die('你没有权限');
            }

            // echo $ac;die;
        }
        // echo $ac;die;

        // 判断当前用户对于当前的请求方法是否允许访问
        
        // 继续后续的请求
        return $next($request);
    }
}
