<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Profession;
use Input;

class ProfessionController extends Controller
{
    //显示专业列表
    public function index(){
    	$data = Profession::get();
    	return view('admin.profession.index',compact('data'));
    }

    // 添加
    public function add(){
    	if(Input::method() == 'POST'){
            // var_dump(Input::all());die;
    		$data = Input::except('_token','file');
    		$data['teachers_ids'] = implode(',',$data['teacher_id']);
    		unset($data['teacher_id']);
    		$data['created_at'] = date('Y-m-d H:i:s');
    		$result = Profession::insert($data);
            return $result ? '1' : '2';
            // var_dump($result);die;
    		// var_dump(Input::all());die;
    	}else{
    		// 获取分类
	    	$protype = \App\Admin\Protype::all();
	    	// 获取老师信息
	    	$teachers = \App\Admin\Member::where('type','2') -> limit(10) -> get();
	    	// 展示视图
	    	return view('admin.profession.add',compact('protype','teachers'));
    	}
    }
}
