<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    //index方法
    public function index(){
    	return view('admin.index.index');
    }
    // welcome框架页面的
    public function welcome(){
    	return view('admin.index.welcome');
    }
}
