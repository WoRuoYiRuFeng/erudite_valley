<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Lession;
use Input;


class LessionController extends Controller
{
    //点播列表
    public function index(){
    	// 查询数据
    	$data = Lession::orderBy('sort','desc') -> get();
    	// 展示视图
    	return view('admin.lession.index',compact('data'));
    }

    // 播放视频
    public function play(){
    	// 获取视频id
    	$video_id = Input::get('id');
    	// 获取视频播放地址
    	$video_addr = Lession::where('id',$video_id) -> value('video_addr');
    	return "<video src='$video_addr' controls autoplay width='100%' height='95%'>你的浏览器不支持视频播放</video>";


    }
}
