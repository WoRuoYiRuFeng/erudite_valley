<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// 引入auth门面
use Auth;

class PublicController extends Controller
{
    //展示登录界面
    public function login(){
    	// 展示登录界面
    	return view('admin.public.login');
    }

    // 验证用户
    public function checkLogin(Request $request){
    	// 自动验证
    	$this -> validate($request,[
    		// 验证的字段	->	规则1|规则2|
    		// 用户名：必填，最少3位，最长20位
    		'username'	=> 'required|min:3|max:20',
    		// 密码：必填，最少6位，最长30,
    		'password'	=> 'required|min:6|max:30',
    		// 验证码：必填，固定5位，必须合法
    		'captcha'	=> 'required|size:5|captcha'
    	]);
    	// 获取用户名和密码
    	$data = $request -> only('username','password');
    	$data['status'] = '2';	// 用户状态
    	// dd($request -> get('online'));
    	// 开始用户信息的认证，记住我
    	$result = Auth::guard('admin') -> attempt($data,$request -> get('online'));
    	// 跳转302的相应
    	if($result) {
    		return redirect('admin/index/index');
    	}else {
    		return redirect('admin/public/login') -> withErrors([
    			'loginError'	=>	'用户名或密码错误'
    		]);
    	}
    	// dd($result);
    }

    // 退出方法
    public function logout(){
    	// 清空session信息
    	Auth::guard('admin') -> logout();
    	// 跳转到登录页面
    	return redirect('/admin/public/login');
    }
}
