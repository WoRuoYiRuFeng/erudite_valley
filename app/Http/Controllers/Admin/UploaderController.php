<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Storage;


class UploaderController extends Controller
{

    //webuploader 方法
    public function webuploader(Request $request){
    	// 1、判断是否上传成功
    	if($request -> hasfile('file') && $request -> file('file') -> isValid()){
	    	// 2、重命名文件
    		$filename = sha1($request -> file('file') -> getClientOriginalName() . time() . rand(1000,9999)) . '.' . $request -> file('file') -> getClientOriginalExtension();
	    	// 3、保存上传文件（临时文件路径）
    		// Storage::disk(磁盘名称)->put(文件路径, 内容);
    		Storage::disk('public')->put($filename, file_get_contents($request -> file('file') ->path()));
    		// 4、给前台一个相应
	    	return response() -> json([
	    		'errorCode'		=> '0',
	    		'message'		=> '文件上传成功',
	    		'path'			=> '/storage/' . $filename
	    	]);
    	}else{
    		return response() -> json([
	    		'errorCode'		=> '1',
	    		'message'		=> $request -> file('file') -> getErrorMessage(),
	    	]);
    	}

    }


    //qiniu 方法
    public function qiniu(Request $request){
    	// 1、判断是否上传成功
    	if($request -> hasfile('file') && $request -> file('file') -> isValid()){
	    	// 2、重命名文件
    		$filename = sha1($request -> file('file') -> getClientOriginalName() . time() . rand(1000,9999)) . '.' . $request -> file('file') -> getClientOriginalExtension();
	    	// 3、保存上传文件（临时文件路径）
    		// Storage::disk(磁盘名称)->put(文件路径, 内容);
    		Storage::disk('qiniu')->put($filename, file_get_contents($request -> file('file') ->path()));
    		// 4、给前台一个相应
	    	return response() -> json([
	    		'errorCode'		=> '0',
	    		'message'		=> '文件上传成功',
                //$disk->getDriver()->downloadUrl('file.jpg')
	    		'path'			=>  Storage::disk('qiniu') -> getDriver() -> downloadUrl($filename)
	    	]);
    	}else{
    		return response() -> json([
	    		'errorCode'		=> '1',
	    		'message'		=> $request -> file('file') -> getErrorMessage(),
	    	]);
    	}

    }
}
