<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// 引入Role模型
use App\Admin\Role;
// 引入Input门面
use Input;

class RoleController extends Controller
{
    //角色列表
    public function index(){
    	// 获取数据
    	$roles = Role::all();
    	// 展示视图
    	return view('admin.role.index',compact('roles'));
    }

    // 角色权限分配
    public function assign(){
    	// 判断请求类型
    	if(Input::method() =='POST'){
    		// 接收数据,数组
    		$data = Input::except('_token');
    		// 实例化模型
    		// var_dump($data);
    		$role = new Role();
    		// 调用自定义模型中的方法实现数据的保存（修改）
    		// int类型的可以直接返回
    		return $role -> assignAuth($data);
    	}else{
    		// 展示视图（先不考虑角色权限）
	    	// 获取全部顶级权限
	    	$top = \App\Admin\Auth::where('pid','0') -> get();
	    	// 获取非顶级的权限,pid不等于0：<> ,>,!=
	    	$category = \App\Admin\Auth::where('pid','<>','0') -> get();
	    	// 获取当前角色已经具备的权限
	    	$roleAuth = Role::where('id',Input::get('id')) -> value('auth_ids');
	    	return view('admin.role.assign',compact('top','category','roleAuth'));
    	}
    }
}
