<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// 引入question模块
use App\Admin\Question;
// 引入Excel插件
use Excel;
use Input;


class QuestionController extends Controller
{
    //列表
    public function index(){
    	$data = Question::all();
    	// 展示视图
    	return view('admin.question.index',compact('data'));
    }

    // 导出
    public function export(){
    	// 定义二维数组（输出的数据）
    	$data = Question::all();
    	// 定制表头
    	$cellData[] = ['序号','题干','所属试卷','分值','选项','正确答案','添加时间'];
    	foreach($data as $key => $value){
    		$cellData[] = [
    			$value -> id,
    			$value -> question,
    			$value -> paper -> paper_name,
    			$value -> score,
    			$value -> options,
    			$value -> answer,
    			$value -> created_at,
    		];
    	}
    	// 创建excel文件（文件名，Excel的实例 use 需要使用的数据）
    	Excel::create(sha1(time() . rand(1000,9999)),function($excel) use ($cellData){
    		// 创建工作表（工作表名称，工作表的实例 use 需要使用的数据）
			$excel->sheet('题库', function($sheet) use ($cellData){
				// 行数据
				$sheet->rows($cellData);
			});
		})->export('xls');
    }

    // 导入
    public function import(){
        if(Input::method() == 'POST'){
            //Excel文件导入功能 By Laravel学院
            // $filePath = 'storage/exports/'.iconv('UTF-8', 'GBK', '学生成绩').'.xls';
            // 获取文件路径
            $filePath = '.' . Input::get('excelfile');
            Excel::load($filePath, function($reader) {
                // 获取当前文件中第一个工作表的数据
                $data = $reader->getSheet('0') -> toArray();

                foreach($data as $key => $value){
                    if($key == '0' ){
                        continue;
                    }
                    $cellData[] = [
                        'question'  => $value[0],//题干
                        'paper_id'  => Input::get('paper_id'),//试卷id
                        'score'     => $value[3],//分值
                        'options'   => $value[1],//选项
                        'answer'    => $value[2],//答案
                        'created_at'=> date('Y-m-d H:i:s'),
                    ];
                }
                // var_dump($cellData);die;
                // 插入数据
                $result = Question::insert($cellData);
                echo $result ? '1' : '2';
            });
        }else{
            // 获取当前试卷
            $paper = \App\Admin\Paper::get();
            // 展示视图
            return view('admin.question.import',compact('paper'));
        }
    }
}
