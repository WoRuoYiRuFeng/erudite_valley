<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Admin\Member;
use DB;
use Input;
use Hash;


class MemberController extends Controller
{
    //列表
    public function index(){
    	// 获取数据
    	$data = Member::all();
    	// 展示视图
    	return view('admin.member.index',compact('data'));
    }

    // 添加
    public function add(){
    	if(Input::method() == 'POST'){
            $data = INput::except('uploadfile','file-2','_token','file');
            // 补充数据
            $data['password'] = bcrypt('password');
            // $data['avatar'] ='/admin/avatar/avatar.jpg';
            $data['created_at'] = date('Y-m-d H:i:s',time());
            // 写入
            $result = Member::insert($data);    //返回布尔值
            return $result ? '1' : '2';
        }else{
            // 查询所有的国家
            $country = DB::table('area') -> where('pid','0') -> get();
            // 展示视图
            return view('admin.member.add',compact('country'));
            }
    }


    // 修改
    public function edit(){
        if(Input::method() == 'POST'){
            $data = Input::except('_token','id','avatar');
            $result = Member::where('id',Input::get('id')) -> update($data);
            return $result ? '1' : '2';
        }else{
            $data = Member::where('id',Input::get('id')) -> get();
             // 查询所有的国家
            $country = DB::table('area') -> where('pid','0') -> get();
            // var_dump($data);die;
            return view('admin.member.edit',compact('data','country'));
        }
    }



    // 根据地区的pid获取下属内容
    public function getAreasByPid(){
    	// 获取pid
    	$pid = Input::get('pid');
    	// 查询
    	$data = DB::table('area') -> where('pid',$pid) -> get();
    	return response() -> json($data);
    }
    

    // 修改密码
    public function repassword(){
        // var_dump(bcrypt(Input::get('oldpassword')));die;
        if(Input::method() =='POST'){
            // 获取原始密码,和id一起查找，如果有对应数据着返回true
            $old = Member:: where('id',Input::get('id')) -> get();
            // 判断密码是否相等
            // if(!empty($old[0] -> password) && bcrypt($old[0] -> password) == Input::get('oldpassword')){

            // hash有问题
            if(!empty($old[0] -> password) && Hash::check(Input::get('oldpassword'), $old[0] -> password)){
                // var_dump(bcrypt($old[0] -> password));die;
                // 如果旧密码存在，则获取新密码
                $data = Input::get();
                if($data[0]->newpassword === $data[0]->newpassword2){
                    $result = Member::where('id',Input::get('id')) -> update(['password',$data[0]->newpassword]);
                    return $result ? '1' : '2';
                }else{
                    return '2';
                }
            }
            
            // 不能使用数组，建议使用orWhere
            // $data = Member:: where([
            //     ['id','=',Input::get('id')],
            //     ['password','=',bcrypt(Input::get('oldpassword'))]
            // ]) -> get();
        }else{
            return view('admin.member.repassword');
        }
    }

    // 删除会员
    public function del(){
        $id = Input::get('id');
        $result = Member::where('id',$id) -> delete();
        return $result ? '1' : '2';
    }
}



