<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// 引入模型
use App\Admin\Admin;
use Input;
use App\Admin\Role;

class AdminController extends Controller
{
    //index，展示管理员列表
    public function index() {
    	// 获取数据
    	$data = Admin::all();
    	// 展示模板
    	return view('admin.admin.index',compact('data'));
    }

    // serverSide 展示服务端分页
    public function serverSide($mark = ''){
    	// 判断是否是ajax请求
    	if($mark == 'isAjax') {
    		// 接受排序的字段
    		$orderByIndex = Input::get('order.0.column');	//接受order[0][column]元素的值
    		$orderRule = Input::get('order.0.dir');	//获取order[0][dir]
    		// 定义DT索引与字段的映射关系
    		$_map = [
    			1 => 'id',
    			2 => 'username',
    			3 => 'mobile',
    			4 => 'email',
    			5 => 'role_id',
    			6 => 'created_at',
    			7 => 'status'
    		];
    		$start = Input::get('start');//起始位置
    		$length = Input::get('length');//偏移量	
    		// 获取关键词
    		$keyword = Input::get('search.value');
    		$orderByFieldName = $_map[$orderByIndex];	//获取字段名
    		// ajax请求
    		return response() -> json([
    			'draw'				=> (int) Input::get('draw'),   //请求计数器
    			'recordsTotal'		=> Admin::count(),//数据表中总的记录数
    			'recordsFiltered'	=> Admin::where('username','like',"%$keyword%") ->count(),//被过滤之后的记录数
    			'data'				=> Admin::orderBy($orderByFieldName,$orderRule) -> offset($start) -> limit($length) -> where('username','like',"%$keyword%") -> get()
    		]);
    	} else {
    		// 展示视图
	    	return view('admin.admin.serverSide');
    	}
    }

    // 添加
    public function add(Request $request){
        if(Input::method() == 'POST'){
            // 自动验证
            $this -> validate($request,[
                'username'  => 'required|min:3|max:20',
                'password'  => 'required|min:6|confirmed',
                'status'    => 'required',
                'role_id'   => 'required',
                'mobile'    => 'required|size:11',
                'email'     => 'required|email'
            ]);
            // echo 111;die;
            $data = Input::except('_token','password2','password_confirmation');
            $data['created_at'] = date('Y-m-d H:i:s');
            // var_dump($data);die;
            $result = Admin::insert($data);

            return $result ? '1' : '2';
        }else{
            // 获取所有角色
            $data = Role::get();
            return view('admin.admin.add',compact('data'));
        }
    }

    // 修改
    public function edit(Request $request){
        if(Input::method() == 'POST'){
            // 判断用户数会否输入密码
            // if(!Input::get('password')){
            if(empty(Input::get('password')) && empty(Input::get('password_confirmation'))){
                // 不修改密码
                $data = Input::except('password','password_confirmation','_token','id');              
            }else{
                // 自动验证
                $this -> validate($request,[
                    'username'  => 'required|min:3|max:20',
                    'password'  => 'required|min:6|confirmed',
                    'status'    => 'required',
                    'role_id'   => 'required',
                    'mobile'    => 'required|size:11',
                    'email'     => 'required|email'
                ]);
                // 修改密码
                $data = Input::except('_token','id','password_confirmation');              
            }
            $data['created_at'] = date('Y-m-d H:i:s');
            // 更新数据
            return Admin::where('id',Input::get('id')) -> update($data);


        }else{
            // 根据指定id查找对应数据
            $data = Admin::where('id',Input::get('id')) -> get();
            // 获取所有的角色
            $roles = Role::get();
            return view('admin.admin.edit',compact('data','roles'));
            // var_dump($data);die;
        }
    }

    // 删除
    public function del(){
        // 返回受影响的行数
        $result = Admin::where('id',Input::get('id')) -> delete();
        return $result ? '1' : '2';
    }

    // 删除多行
    public function dels(){
        var_dump(Input::get());die;
    }


}
