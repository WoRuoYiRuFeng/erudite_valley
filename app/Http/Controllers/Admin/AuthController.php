<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
// 引入input
use Input;
use App\Admin\Auth;

class AuthController extends Controller
{
    //展示表单页面
    public function add(){
    	// 请求判断
    	// Input::method(),返回请求的类型：GET、POST
    	if(Input::method() == 'POST'){
    		$result = Auth::insert(Input::except('_token'));
    		// 返回添加的结果
    		return $result ? '1' : '0';
    	} else {
    		$parents = Auth::where('pid','0') -> get();
	    	// 展示视图
	    	return view('admin.auth.add',compact('parents'));
	    }
	}

	// 显示列表
	public function index(){
		$data = Auth::orderBy('pid','asc') -> get();
		return view('admin.auth.index',compact('data'));
	}

    // 修改
    public function edit($id = '') {
        if(Input::method() == 'POST') {
            // 接收表单数据
            $result = Auth::where('id',INput::get('id')) -> update(Input::except('_token','id'));
            return $result ? '1': '2';
            // var_dump($result);die;
        }else{
            // 显示修改表单
            // 通过id获取指定的数据
            $auth = Auth::where('id',$id) -> get();
            // 获取所有的父元素
            $parents = Auth::where('pid',0) -> get();
            return view('admin.auth.edit',compact('auth','parents'));
        }
    }

    // editajax请求判断是否是顶级权限
    // public function editajax(){
    //     var_dump(Input::get('id'));die;
    // }

    // 删除
    public function del(){
        // echo Input::get('id');
        $result = Auth::where('id',Input::get('id')) -> delete();
        return $result ? '1' : '2';
    }
}
