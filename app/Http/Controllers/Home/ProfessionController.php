<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Admin\Profession;
use Input;

class ProfessionController extends Controller
{
    //
    public function index(){
    	// 专业信息
    	$data = Profession::find(Input::get('id'));
    	// 获取任课老师的用户名
    	$teachers = \App\Admin\Member::whereIn('id',explode(',',$data -> teachers_ids)) -> get();
    	// 拼凑老师的用户名
    	$str = '';
    	foreach ($teachers as $key => $value) {
    		$str .= $value -> username . '、';
    	}
    	$data -> teachers = rtrim($str,'、');
    	return view('home.profession.index',compact('data'));
    }

    // 专业购买确认页面
    public function makeOrder(){
    	// dd(date('Y-m-d H:i:s',strtotime('+1 day')));
    	// 根据指定id获取对应数据
    	$data = Profession::find(Input::get('id'));
    	// 展示视图
    	return view('home.profession.makeorder',compact('data'));
    }


    //实现微信支付
    public function pay(){
        //1.获取商品的信息
        $data = Profession::find(Input::get('id'));
        //2.订单信息入库的操作

        //3.使用微信付款
        require_once "./wx/lib/WxPay.Api.php";
        require_once "./wx/example/WxPay.NativePay.php";
        require_once './wx/example/log.php';
        $notify = new \NativePay();
        $input = new \WxPayUnifiedOrder();
        //设置商品描述【是  String(128)】
        $input->SetBody("在线教育专业购买：" . $data -> pro_name);
        //设置附加数据【否  String(127)】
        $input->SetAttach("test");
        //设置商户订单号，商家自己的表中订单号，不能重复【是 String(32)】
        $input->SetOut_trade_no(\WxPayConfig::MCHID.date("YmdHis"));
        //设置订单价格，单位为分【是 Int】
        $input->SetTotal_fee($data -> price * 0.0001 * 100);
        //设置交易起始时间【否    String(14)】
        $input->SetTime_start(date("YmdHis"));
        //设置交易结束时间【否    String(14)】
        $input->SetTime_expire(date("YmdHis", time() + 1800));
        //设置订单优惠标记【否    String(32)】
        $input->SetGoods_tag("test");
        //设置通知地址【是  String(256)】
        $input->SetNotify_url("http://paysdk.weixin.qq.com/example/notify.php");
        //设置交易类型【是  String(16)】
        $input->SetTrade_type("NATIVE");
        //设置商品ID【否  String(32)】
        $input->SetProduct_id($data -> id);
        //将刚才输入的全部信息进行打包作为获取QR必要参数
        $result = $notify->GetPayUrl($input);
        $url2 = $result["code_url"];
        //4.输出二维码
        echo "<img alt='模式二扫码支付' src='http://paysdk.weixin.qq.com/example/qrcode.php?data=" . urlencode($url2) . "' style='width:150px;height:150px;'/>";
    }

    // 自定义的函数
    public function duanxin(){

        $str = "http://v.juhe.cn/sms/send?mobile=156613337514&tpl_id=53135&tpl_value=%23code%23%3D53135&key=d36c1d9829185050b5ac21d5beafa4a8";
        return $str;
    }

}
