<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Admin\Live;
use Input;

class LiveController extends Controller
{
    //直播播放页面
    public function index(){
    	// 拉流地址
    	$url = 'rtmp://pili-live-rtmp.28sjw.com/education-zet';
    	// 根据直播课程id获取stream_id，进而获取stream名称
    	$data = Live::find(Input::get('id'));

        // dd($data);
    	$url .= $data -> stream -> stream_name;
    	// 展示页面
    	return view('home.live.index',compact('url','data'));
    }
}
