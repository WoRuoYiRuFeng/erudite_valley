<?php

namespace App\Http\Controllers\Home;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use DB;

class IndexController extends Controller
{
    //展示视图
    public function index(){
    	// 查询直播的数据（live）,显示状态正常的
    	$live = DB::table('live') -> where('status','1') -> get() -> toArray();
    	// 判断直播状态
    	foreach ($live as $key => $value) {
    		// 判断状态
    		if(time() > $value -> end_at){
    			// 已结束:当前时间大于结束时间
    			$value -> liveStatus = '直播已结束';
    		}elseif ($value -> begin_at < time() && time() < $value -> end_at) {
    			// 直播中
    			$value -> liveStatus = '正在直播中';
    		}else{
    			// 直播未开始
    			$value -> liveStatus ='直播未开始';
    		}
    	}
    	

    	// 获取相关专业(profession)
    	$profession = DB::table('profession') -> orderBy('sort','desc') -> get();
    	// 展示视图
    	return view('home.index.index',compact('live','profession'));
    }


}
