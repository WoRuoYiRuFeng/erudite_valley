<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Profession extends Model
{
    //定义关联的数据表
    protected $table = 'profession';

    // 定义与专业分类的一对一关系
    public function protype(){
    	// 关联模型，关联id，本地id
    	return $this -> hasOne('App\Admin\Protype','id','protype_id');
    }
}
