<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;
// 引入系统引进实现好的接口空间
use Illuminate\Auth\Authenticatable;

class Admin extends Model implements \Illuminate\Contracts\Auth\Authenticatable
{
    // 定义表名
    protected $table = 'admin';
    // 引用Authenticatable代码段
    use Authenticatable;


    // 定义与角色之间的一对一的关系
    public function role(){
    	// 关系表模型，关系表id，本地id
    	return $this -> hasOne('App\Admin\Role','id','role_id');
    }

}
