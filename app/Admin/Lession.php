<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Lession extends Model
{
    //定义关联的表名
    protected $table = 'lession';

    // 关联关系
    public function course(){
    	return $this -> hasOne('App\Admin\Course','id','course_id');
    }
}
