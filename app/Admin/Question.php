<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    protected $table = 'question';

    // 定义与课程表的一对一关系
    public function paper(){
    	// 关联表模型，外表id，本地id
    	return $this -> hasOne('App\Admin\Paper','id','paper_id');
    }
}
