<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //定义关联的数据表
    protected $table = 'role';
    // 禁用时间字段
    public $timestamps = false;

    // assignAuth方法处理权限
    public function assignAuth($data) {
    	// 获取auth_ids字段，转换为字符串
    	$post['auth_ids'] = implode(',',$data['auth_id']);
    	// 查询auth表中的对应的记录
    	$auth = Auth::where('pid', '>','0') -> whereIn('id',$data['auth_id']) -> get();
    	// var_dump($auth->toArray());die;
    	$ac = '';
    	// 拼接a和c
    	foreach ($auth as $key => $value) {
    		$ac .= $value -> controller . '@' .$value -> action . ','; 
    	}
    	// 去除右边多余的逗号
    	$post['auth_ac'] = rtrim($ac,',');
    	// var_dump($post);die;
    	// 将数据存储，并返回（返回受影响的行数）
    	return Role::where('id',$data['id']) -> update($post);
    	// var_dump($ac);
    }
}
