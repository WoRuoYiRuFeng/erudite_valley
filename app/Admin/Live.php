<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Live extends Model
{
    // 定义关联的数据表
    protected $table = 'live';

    // 关联点播列表（一对一）
    public function profession(){
    	return $this -> hasOne('App\Admin\Profession','id','profession_id');
    }

    // 关联流（一对一）
    public function stream(){
    	return $this -> hasOne('App\Admin\Stream','id','stream_id');
    }
}
