<?php

namespace App\Admin;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    //定义关联的数据表
    protected $table = 'course';

    // 定义与专业相关的一对一
    public function profession(){
    	return $this -> hasOne('App\Admin\Profession','id','profession_id');
    }
}
