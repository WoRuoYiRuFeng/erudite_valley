<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <!--[if lt IE 9]>
<script type="text/javascript" src="/admin/lib/html5shiv.js"></script>
<script type="text/javascript" src="/admin/lib/respond.min.js"></script>
<![endif]-->
    <link rel="stylesheet" type="text/css" href="/admin/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="/admin/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="/admin/lib/Hui-iconfont/1.0.8/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="/admin/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="/admin/static/h-ui.admin/css/style.css" />
    <!--[if IE 6]>
<script type="text/javascript" src="/admin/lib/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
    <title>修改权限 - 权限管理 - H-ui.admin v3.1</title>
    <meta name="keywords" content="H-ui.admin v3.1,H-ui网站后台模版,后台模版下载,后台管理系统模版,HTML后台模版下载">
    <meta name="description" content="H-ui.admin v3.1，是一款由国人开发的轻量级扁平化网站后台模板，完全免费开源的网站后台管理系统模版，适合中小型CMS后台系统。">
</head>

<body>
    <article class="page-container">
        <form class="form form-horizontal" id="form-admin-edit">
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>权限名称：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="{{$auth[0]->auth_name}}" placeholder="请输入权限名称" id="auth_name" name="auth_name">
                </div>
            </div>
           <!--  @if($auth[0]->pid == '0') <div style="display: none" class="zidingyi">
            @else <div  class="zidingyi"> 
            @endif -->
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>控制器名称：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="{{$auth[0]->controller}}" placeholder="" id="controller" name="controller">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>方法名称：</label>
                <div class="formControls col-xs-8 col-sm-9">
                    <input type="text" class="input-text" value="{{$auth[0]->action}}" placeholder="" name="action" id="action">
                </div>
            </div>
            <!-- </div> -->
            
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3">父级权限：</label>
                <div class="formControls col-xs-8 col-sm-9"> <span class="select-box" style="width:150px;">
			<select class="select" name="pid" size="1">
				<option value="0">作为顶级权限</option>
				@foreach($parents as $val)
					<option value="{{$val -> id}}" @if($auth[0]->pid == $val->id) selected @endif >{{$val -> auth_name}}</option>
				@endforeach
			</select>
			</span> </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>作为导航：</label>
                <div class="formControls col-xs-8 col-sm-9 skin-minimal">
                    <div class="radio-box">
                        <input name="is_nav" type="radio" value="1" id="is_nav-1" @if($auth[0]->is_nav =='1') checked @endif >
                        <label for="is_nav-1">是</label>
                    </div>
                    <div class="radio-box">
                        <input type="radio" id="is_nav-2" value="2" name="is_nav" @if($auth[0]->is_nav =='2') checked @endif >
                        <label for="is_nav-2">否</label>
                    </div>
                </div>
            </div>
            {{csrf_field()}}
            <input type="hidden" value="{{$auth[0]->id}}" name="id">
            <div class="row cl">
                <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
                    <input class="btn btn-primary radius" type="submit" value="&nbsp;&nbsp;提交&nbsp;&nbsp;">
                </div>
            </div>
        </form>
    </article>
    <!--_footer 作为公共模版分离出去-->
    <script type="text/javascript" src="/admin/lib/jquery/1.9.1/jquery.min.js"></script>
    <script type="text/javascript" src="/admin/lib/layer/2.4/layer.js"></script>
    <script type="text/javascript" src="/admin/static/h-ui/js/H-ui.min.js"></script>
    <script type="text/javascript" src="/admin/static/h-ui.admin/js/H-ui.admin.js"></script>
    <!--/_footer 作为公共模版分离出去-->
    <!--请在下方写此页面业务相关的脚本-->
    <script type="text/javascript" src="/admin/lib/jquery.validation/1.14.0/jquery.validate.js"></script>
    <script type="text/javascript" src="/admin/lib/jquery.validation/1.14.0/validate-methods.js"></script>
    <script type="text/javascript" src="/admin/lib/jquery.validation/1.14.0/messages_zh.js"></script>
    <script type="text/javascript">

    $(function() {
        // $.ajax({
        //     type:'type',
        //     url:'admin/admin/editajax'
        //     data:{id:id},
        //     success:function(data){
        //         echo data;
        //         // 如果为1表示顶级
        //         if(data == '1'){
        //             // 当页面首次加载的时候需要隐藏控制器和方法的表单
        //             $('#controller,#action').parent().parent().hide();
        //         }
        //     }
        // });
    	// 当页面首次加载的时候需要隐藏控制器和方法的表单
        // $('#controller,#action').parent().parent().hide();
    	// 当权限发生变化的时候
    	$('select').change(function(){
    		// 获取值，判断值是否为0
    		var _val = $(this).val();
    		// 在切换的时候为了避免值会保留，则需要清空
    		$('#controller,#action').val('');
            // 如果值为0，就是顶级
    		if(_val == 0) {
                // $('.zidingyi').hide(500);
    			$('#controller,#action').parent().parent().hide(500);
    		}else {
    			$('#controller,#action').parent().parent().show(500);
    		}
    	});


        $('.skin-minimal input').iCheck({
            checkboxClass: 'icheckbox-blue',
            radioClass: 'iradio-blue',
            increaseArea: '20%'
        });

        $("#form-admin-edit").validate({
            rules: {
                auth_name: {
                    required: true,
                    minlength: 4,
                    maxlength: 20
                },
                is_nav: {
                    required: true,
                },
                controller: {
                    required: true,
                },
                action: {
                    required: true,
                },
                pid: {
                    required: true,
                },
            },
            onkeyup: false,
            focusCleanup: true,
            success: "valid",
            submitHandler: function(form) {
            	// ajax异步请求，索引返回结果必须写if后面
                $(form).ajaxSubmit({
                    type: 'post',
                    url:'',
                    success: function(data) {
                    	if(data == '1'){
                    		layer.msg('修改成功!', { icon: 1, time: 1000 },function(){
                                // 在返回成功之后刷新当前页
                                window.parent.location.reload();
                    			var index = parent.layer.getFrameIndex(window.name);
				                parent.$('.btn-refresh').click();
				                parent.layer.close(index);
                    		});
                    	} else {
                    		layer.msg('修改失败!', { icon: 2, time: 1000 });
                    	}
                    },
                    error: function(XmlHttpRequest, textStatus, errorThrown) {
                        layer.msg('error!', { icon: 2, time: 1000 });
                    }
                });
                
            }
        });
    });
    </script>
    <!--/请在上方写此页面业务相关的脚本-->
</body>

</html>