<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('admin',function(Blueprint $table){
            // 设计字段
            $table -> increments('id');  //自增主键
            $table -> string('username',20) -> notNull();   //用户名
            $table -> string('password') -> notNull();  // 密码
            $table -> enum('gender',['1','2','3']) -> default('1');   //性别
            $table -> string('mobile',11);  //手机号
            $table -> string('email',50);   //邮箱
            $table -> tinyInteger('role_id');//角色id
            $table -> timestamps(); //自动添加create_at 和 update_at 字段，datetime类型
            $table -> rememberToken();  //记住登录功能需要的字段
            $table -> enum('status',['1','2']) -> default('2');//账号状态，默认启用

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('admin');
    }
}
