<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('member',function($table){
        $table -> increments('id'); 
        $table -> string('username',20) -> notNull();
        $table -> string('password') -> notNull();
        $table -> enum('gender',[1,2,3]) -> nutNull() -> default('1');
        $table -> string('mobile',11);
        $table -> string('email',40);
        $table -> string('avatar');
        // 针对四级联动需要地址的存储字段
        $table -> integer('country_id');    //国家id
        $table -> integer('province_id');   //省id
        $table -> integer('city_id');       //市id
        $table -> integer('ccounty_id');    //县id
        $table -> timestamps();
        $table -> rememberToken();
        $table -> enum('type',[1,2]) -> notNull() -> default('1');
        $table -> enum('status',[1,2]) -> notNull() -> default('2');
    });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('member');
    }
}
