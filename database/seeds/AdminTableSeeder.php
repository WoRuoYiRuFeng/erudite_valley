<?php

use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        // 生成100条测试数据
        $data = [];
        // 创建Faker实例
        $faker = \Faker\Factory::create('zh_CN');
        for($i = 0; $i < 100; $i++) {
        	//每次生成一条数据
        	$data[] = [
        		'username'		=> $faker -> userName,	//用户名
        		'password'		=> bcrypt('123456'),	//使用laravel框架生成密码
        		'gender'		=> rand(1,3),	//性别
        		'mobile'		=> $faker -> phoneNumber,//手机
        		'email'			=> $faker -> email,//生成邮箱
        		'role_id'		=> rand(1,5),//角色
        		'created_at'		=> date('Y-m-d H:i:s'),//创建时间
        		'status'		=> rand(1,2)	//状态
        	];
        }
        // 一次性写入数据
        DB::table('admin') -> insert($data);
    }
}
