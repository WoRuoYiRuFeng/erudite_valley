<?php

use Illuminate\Database\Seeder;

class PaperTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('paper') -> insert([
		    'paper_name'		=>	'ThinkPHP电子商城阶段考试',
		    'course_id'			=>	3,
		    'created_at'		=>	date('Y-m-d H:i:s')
		]);
		DB::table('paper') -> insert([
		    'paper_name'		=>	'jQuery阶段考试',
		    'course_id'			=>	2,
		    'created_at'		=>	date('Y-m-d H:i:s')
		]);
		DB::table('paper') -> insert([
		    'paper_name'		=>	'linux阶段考试',
		    'course_id'			=>	1,
		    'created_at'		=>	date('Y-m-d H:i:s')
		]);
		DB::table('paper') -> insert([
		    'paper_name'		=>	'laravel阶段考试',
		    'course_id'			=>	4,
		    'created_at'		=>	date('Y-m-d H:i:s')
		]);
    }
}
