<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// 后台登录路由
Route::get('/admin/public/login','Admin\PublicController@login') -> name('login');
// 登录表单的处理
Route::post('/admin/public/checkLogin','Admin\PublicController@checkLogin');
// 退出登录
Route::get('/admin/public/logout','Admin\PublicController@logout');

// 后台路由(登录判断，rbac权限判断)
Route::group(['prefix' => 'admin','middleware' => ['auth:admin','checkrbac']],function(){
	
	// 展示后台首页
	Route::get('index/index','Admin\IndexController@index');
	Route::get('index/welcome','Admin\IndexController@welcome');

	// 管理员管理模块
	Route::get('admin/index','Admin\AdminController@index');	//列表
	Route::any('admin/add','Admin\AdminController@add');		//添加
	Route::post('admin/del','Admin\AdminController@del');		//删除
	Route::post('admin/dels','Admin\AdminController@dels');		//删除
	Route::any('admin/edit','Admin\AdminController@edit');		//修改
	Route::any('admin/editajax','Admin\AdminController@editajax');		//修改
	Route::get('admin/serverSide/{mark?}','Admin\AdminController@serverSide');//服务端分页

	// 会员管理
	Route::get('member/index','Admin\MemberController@index');	//列表	
	Route::any('member/add','Admin\MemberController@add');		//添加
	Route::post('member/del','Admin\MemberController@del');		//删除
	Route::any('member/edit','Admin\MemberController@edit');	//修改
	Route::any('member/repassword','Admin\MemberController@repassword');		//修改密码
	Route::get('member/getAreasByPid','Admin\MemberController@getAreasByPid');	//ajax请求四级联动

	// 权限
	Route::any('auth/add','Admin\AuthController@add');			//添加
	Route::get('auth/index','Admin\AuthController@index');		//列表
	Route::post('auth/del','Admin\AuthController@del');		//列表
	Route::any('auth/edit/{id?}','Admin\AuthController@edit');	//修改

	// 角色
	Route::get('role/index','Admin\RoleController@index');		//列表
	Route::any('role/assign','Admin\RoleController@assign');	//权限分配

	// 文件上传
	Route::post('uploader/webuploader','Admin\UploaderController@webuploader');
	Route::post('uploader/qiniu','Admin\UploaderController@qiniu');	//七牛

	// 专业分类
	Route::get('protype/index','Admin\ProtypeController@index');		//分类列表
	Route::any('protype/add','Admin\ProtypeController@add');		//分类列表
	
	// 专业管理	                                                            		
	Route::get('profession/index','Admin\ProfessionController@index');	//专业列表
	Route::any('profession/add','Admin\ProfessionController@add');		//专业添加
	
	// 课程和点播的管理操作
	Route::get('course/index','Admin\CourseController@index');	//课程列表
		                                                          	
	Route::get('lession/index','Admin\LessionController@index');//点播列表
	Route::get('lession/play','Admin\LessionController@play');	//播放视屏
	Route::any('lession/add','Admin\LessionController@add');	//添加
	Route::any('lession/edit','Admin\LessionController@edit');	//点播列表
	Route::post('lession/del','Admin\LessionController@del');	//点播列表

	// 试卷和试题
	Route::get('paper/index','Admin\PaperController@index');			//试卷列表
	Route::get('question/index','Admin\QuestionController@index');		//试题列表
	Route::get('question/export','Admin\QuestionController@export');	//导出
	Route::any('question/import','Admin\QuestionController@import');	//导入

	// 直播和直播流的管理
	Route::get('stream/index','Admin\StreamController@index');	//导出
	Route::any('stream/add','Admin\StreamController@add');	//添加
	Route::get('live/index','Admin\LiveController@index');	//直播列表

 });



// ----------------------------------------------------------------------------

// 定义前台路由
Route::get('/','Home\IndexController@index');	//跟路由

// 直播观看路由
Route::get('home/live','Home\LiveController@index');	

// 专业详情
Route::get('home/profession','Home\ProfessionController@index');	

// 专业订单确认页面
Route::get('home/makeOrder','Home\ProfessionController@makeOrder');	
                                             	
Route::get('home/profession/pay','Home\ProfessionController@pay');

// 发送短信
Route::get('home/profession/duanxin','Home\ProfessionController@duanxin');
